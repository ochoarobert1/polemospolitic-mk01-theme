<footer class="container-fluid" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
    <div class="row">
        <div class="the-footer col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="footer-item col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <h5>Información</h5>
                        <p><i class="fa fa-map-marker"></i> <?php echo get_option('polemospolitic_dir'); ?></p>
                        <p><i class="fa fa-phone"></i> <?php echo get_option('polemospolitic_telf'); ?></p>
                        <p><i class="fa fa-envelope"></i> <a href="mailto:<?php echo get_option('polemospolitic_email'); ?>"><?php echo get_option('polemospolitic_email'); ?></a></p>
                    </div>
                    <div class="footer-item col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <h5>Enlaces de Interés</h5>
                        <?php wp_nav_menu(array('container_class' => 'menu-footer', 'theme_location' => 'footer_menu')); ?>
                    </div>
                     <div class="footer-item col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <?php dynamic_sidebar( 'footer_sidebar' ); ?>
                    </div>

                </div>
            </div>
        </div>
        <div class="footer-copy col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="footer-copy-info col-md-9">
                        <?php _e('2017 - Polemos Politic | Todos los derechos reservados', 'polemospolitic'); ?> - J-403170320
                    </div>
                    <div class="footer-copy-extra col-md-3">
                        <a href="http://robertochoa.com.ve/?utm_source=footer&utm_medium=link&utm_content=polemos" title="<?php _e('Sitio desarrollado por Robert Ochoa', 'polemospolitic'); ?>"><?php _e('Sitio desarrollado por Robert Ochoa', 'polemospolitic'); ?></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php wp_footer() ?>
</body>
</html>
