<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <?php $images = rwmb_meta( 'rw_page_banner', 'size=full' );  ?>
        <?php if ( !empty( $images ) ) { ?>
        <?php foreach ( $images as $image ) { $full_url = $image['full_url']; } ?>
        <?php } ?>
        <div class="page-banner col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr animated fadeIn" style="background: url(<?php echo $full_url; ?>);">
            <div class="page-banner-wrapper"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 itemprop="headline"><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
            <div class="the-breadcrumbs col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php echo the_breadcrumb(); ?>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <section class="page-container col-lg-12 col-md-12 col-sm-12 col-xs-12" role="article" itemscope itemtype="http://schema.org/BlogPosting">
                    <article id="post-<?php the_ID(); ?>" class="page-content <?php echo join(' ', get_post_class()); ?>" >
                        <div class="page-article col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr" itemprop="articleBody">
                            <?php the_content(); ?>
                            <div class="clearfix"></div>
                            <?php $group_value = rwmb_meta( 'nosotros_group' ); ?>
                            <?php if ($group_value) { ?>
                            <div class="members-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                <h2><?php _e('NUESTRO EQUIPO', 'polemospolitic'); ?></h2>
                                <?php $i = 1; ?>
                                <?php foreach ($group_value as $group_item) { ?>

                                <?php foreach ($group_item['rw_pic_member'] as $item) { ?>
                                <?php $feat_image_url = wp_get_attachment_image( $item, 'full', "", array( "class" => "img-responsive" ) );  ?>
                                <?php } ?>
                                <div class="member-item member-item-<?php echo $i; ?> col-lg-4 col-md-4 col-sm-4 col-xs-12 wow delay-<?php echo $i; ?> no-paddingl no-paddingr">

                                    <picture class="member-picture col-md-12">
                                        <?php echo $feat_image_url; ?>
                                    </picture>

                                    <div class="member-item-info col-md-12">
                                        <h2><?php echo $group_item['rw_name_member']; ?></h2>
                                        <?php echo $group_item['rw_desc_member']; ?>
                                    </div>


                                </div>
                                <?php $i++; } ?>
                            </div>
                            <?php } ?>
                        </div>
                    </article>
                </section>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>
