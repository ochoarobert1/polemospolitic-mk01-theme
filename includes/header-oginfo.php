<meta name="title" content="<?php if (is_home()) { echo get_bloginfo('name') . ' | ' . get_bloginfo('description'); } else { echo get_bloginfo('name') . ' | ' . get_the_title(); } ?>">
<?php global $wp_query; ?>
<?php $current_url = $wp_query->query_vars['name']; ?>
<?php if(is_single()) : ?>
<?php echo get_custom_metadata_info($current_url, 'description'); ?>
<?php endif; ?>
<?php if (is_home() ) { echo '<meta name="description" content="'. get_bloginfo("name") .' | ' . get_bloginfo("description") . '">'; } ?>
<?php if (is_page() ) { echo '<meta name="description" content="'. get_bloginfo("name") .' | ' . get_bloginfo("description") . '">'; } ?>
<?php if(is_single()) : ?>
<?php echo get_custom_metadata_info($current_url, 'keywords'); ?>
<?php endif; ?>
<?php if (is_home() || is_page() ) { echo '<meta name="keywords" content="Polemos Politic, consultor politico, consultor empresarial, coaching estratégico, formación profesional, organización política" />'; } ?>
<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="@PolemosPolitic" />
<meta name="twitter:creator" content="@PolemosPolitic" />
<meta property='fb:admins' content='100000133943608' />
<meta property="fb:app_id" content="826325807533626" />
<meta property="og:title" content="<?php bloginfo('name'); ?> | <?php is_front_page() ? bloginfo('description') : wp_title(''); ?>" />
<meta property="og:site_name" content="Polemos Politic" />
<meta property="og:type" content="article" />
<meta property="og:locale" content="es_ES" />
<meta property="og:url" content="<?php if(is_single()) { the_permalink(); } else { echo 'http://www.polemospolitic.com'; }?>" />
<?php if(is_single()) :  ?>
<?php echo get_custom_metadata_info($current_url, 'og:description'); ?>
<?php endif; ?>
<?php if (is_home() ) { echo '<meta property="og:description" content="'. get_bloginfo("name") .'" | "' . get_bloginfo("description") . '">'; } ?>
<?php if (is_page() ) { echo '<meta property="og:description"  content="'. get_bloginfo("name") .'" | "' . get_bloginfo("description") . '">'; } ?>
<meta property="og:image" content='<?php if(is_single()){ $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID) ); echo $url; } else { echo esc_url( get_template_directory_uri() ) ."/images/oglogo.jpg"; } ?>' />
