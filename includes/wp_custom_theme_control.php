<?php
/* --------------------------------------------------------------
CUSTOM AREA FOR OPTIONS DATA - polemospolitic
-------------------------------------------------------------- */

/* CUSTOM MENU PAGE AND FUNCTIONS IN ADMIN */
function register_polemospolitic_settings() {
    //register our settings
    register_setting( 'polemospolitic-settings-group', 'polemospolitic_dir' );
    register_setting( 'polemospolitic-settings-group', 'polemospolitic_email' );
    register_setting( 'polemospolitic-settings-group', 'polemospolitic_telf' );
    register_setting( 'polemospolitic-settings-group', 'polemospolitic_fb' );
    register_setting( 'polemospolitic-settings-group', 'polemospolitic_tw' );
    register_setting( 'polemospolitic-settings-group', 'polemospolitic_ig' );
    register_setting( 'polemospolitic-settings-group', 'polemospolitic_yt' );
}

function my_admin_menu() {
    add_menu_page( 'Opciones del Sitio', 'Opciones del Sitio', 'manage_options', 'polemospolitic_custom_options', 'my_custom_menu_page', get_template_directory_uri() . '/images/plugin-icon.png', 120  );
    /* call register settings function */
    add_action( 'admin_init', 'register_polemospolitic_settings' );
}

add_action( 'admin_menu', 'my_admin_menu' );



/* CUSTOM CSS FOR THIS SECTION */
function load_custom_wp_admin_style($hook) {
    if( $hook != 'toplevel_page_polemospolitic_custom_options' ) {
        return;
    }
    /* ENQUEUE THE CSS */
    wp_enqueue_style('google-fonts', 'https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700&amp;subset=latin-ext');
    wp_enqueue_style( 'custom_wp_admin_css', get_template_directory_uri() . '/css/custom-wordpress-admin-style.css' );
}

add_action( 'admin_enqueue_scripts', 'load_custom_wp_admin_style' );

/* SEPARATOR FOR STYLING THE CUSTOM PAGE */
function add_admin_custom_menu_separator( $position ) {

    global $menu;

    $menu[ $position ] = array(
        0    =>    '',
        1    =>    'read',
        2    =>    'separator' . $position,
        3    =>    '',
        4    =>    'wp-menu-separator'
    );

}

function set_admin_custom_menu_separator() {  do_action( 'admin_init', 119 );  }

add_action( 'admin_init', 'add_admin_custom_menu_separator' );
add_action( 'admin_menu', 'set_admin_custom_menu_separator' );

/* CUSTOM MENU PAGE CONTENT */
function my_custom_menu_page() { ?>

<div class="polemospolitic_custom_options-header">
    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.png" alt="<?php echo get_bloginfo('name'); ?>" class="logo-header" />
    <h1><?php echo get_admin_page_title(); ?></h1>
</div>
<div class="polemospolitic_custom_options-content">
    <form method="post" action="options.php">
        <?php settings_fields( 'polemospolitic-settings-group' ); ?>
        <?php do_settings_sections( 'polemospolitic-settings-group' ); ?>
        <table class="form-table">

            <tr valign="top">
                <th scope="row"><?php _e('Dirección', 'polemospolitic'); ?></th>
                <td><textarea name="polemospolitic_dir" cols="95" rows="5"><?php echo esc_attr( get_option('polemospolitic_dir') ); ?></textarea></td>
            </tr>

            <tr valign="top">
                <th scope="row"><?php _e('Correo Electrónico', 'polemospolitic'); ?></th>
                <td><input type="text" size="90" name="polemospolitic_email" value="<?php echo esc_attr( get_option('polemospolitic_email') ); ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row"><?php _e('Teléfono', 'polemospolitic'); ?></th>
                <td><input type="text" size="90" name="polemospolitic_telf" value="<?php echo esc_attr( get_option('polemospolitic_telf') ); ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row" colspan="2"><h3><?php _e('Redes Sociales', 'polemospolitic'); ?></h3></th>
            </tr>

            <tr valign="top">
                <th scope="row"><?php _e('Perfil de Facebook', 'polemospolitic'); ?></th>
                <td><input type="text" size="90" name="polemospolitic_fb" value="<?php echo esc_attr( get_option('polemospolitic_fb') ); ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row"><?php _e('Perfil de Twitter', 'polemospolitic'); ?></th>
                <td><input type="text" size="90" name="polemospolitic_tw" value="<?php echo esc_attr( get_option('polemospolitic_tw') ); ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row"><?php _e('Perfil de Instagram', 'polemospolitic'); ?></th>
                <td><input type="text" size="90" name="polemospolitic_ig" value="<?php echo esc_attr( get_option('polemospolitic_ig') ); ?>" /></td>
            </tr>

            <tr valign="top">
                <th scope="row"><?php _e('Canal de Youtube', 'polemospolitic'); ?></th>
                <td><input type="text" size="90" name="polemospolitic_yt" value="<?php echo esc_attr( get_option('polemospolitic_yt') ); ?>" /></td>
            </tr>

        </table>
        <?php submit_button(); ?>
    </form>
</div>
<?php }




