<?php
/* --------------------------------------------------------------
    GET CUSTOM REVSLIDER
-------------------------------------------------------------- */

function get_custom_slider() {
    global $wpdb;
    $the_query = "SELECT title, alias FROM " . $wpdb->prefix . "revslider_sliders";
    $sliders = $wpdb->get_results($the_query, 'ARRAY_A');
    $item_container = [];
    $i = 1;
    if (empty($sliders)) {
        $item_container[] = array(" " => " ");
    } else {
        foreach ($sliders as $item){
            $itemkeys[] = $item['alias'];
            $itemvalues[] = $item['title'];
        }
        $item_container[] = array_combine($itemkeys, $itemvalues);
    }
    return $item_container[0];
}

/* --------------------------------------------------------------
    GET CATEGORIES
-------------------------------------------------------------- */

function get_custom_category() {
    global $wpdb;
    $terms = get_terms( array(
        'taxonomy' => 'category',
        'hide_empty' => false,
    ) );
    $item_container = [];
    $i = 1;
    if ( ! empty( $terms ) && ! is_wp_error( $terms ) ){
        foreach ( $terms as $term ) {
            $itemkeys[] = $term->term_id;
            $itemvalues[] = $term->name;
        }
        $item_container[] = array_combine($itemkeys, $itemvalues);
    } else {
        $item_container[] = array(" " => " ");
    }
    return $item_container[0];
}


/* --------------------------------------------------------------
    DECLARE METABOX
-------------------------------------------------------------- */

add_filter( 'rwmb_meta_boxes', 'polemospolitic_metabox' );

function polemospolitic_metabox( $meta_boxes )
{
    $prefix = 'rw_';

    $meta_boxes[] = array(
        'id'         => 'home_data',
        'title'      => __( 'Secciones de la Pagina / Información Extra', 'polemospolitic' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'include' => array(
            // With all conditions below, use this logical operator to combine them. Default is 'OR'. Case insensitive. Optional.
            'relation'        => 'OR',
            'slug'            => array( 'home', 'inicio' ),
            'template'        => array(  'templates-home.php' ),
        ),
        'fields' => array(
            array(
                'id'   => 'custom_html',
                'type' => 'custom_html',
                'std'  => '<div class="custom-metabox-headline"><h2>' . __('Slider Principal', 'polemospolitic'). '</h2></div>',
                // 'callback' => 'display_warning',
            ),
            array(
                'name'     => __( 'Seleccione Revolution Slider', 'polemospolitic' ),
                'id'       => $prefix . 'slider',
                'type'     => 'select_advanced',
                // Array of 'value' => 'Label' pairs for select box
                'options'  => get_custom_slider(),
                // Select multiple values, optional. Default is false.
                'multiple' => false,
                'std'         => ' ', // Default value, optional
                'placeholder' => __( 'Seleccione un slider', 'polemospolitic' ),
            ),
            array(
                'id'   => 'custom_html',
                'type' => 'custom_html',
                'std'  => '<div class="custom-metabox-headline"><h1>' . __('Secciones del Home', 'polemospolitic'). '</h1></div>',
                // 'callback' => 'display_warning',
            ),
            array(
                'id'   => 'custom_html',
                'type' => 'custom_html',
                'std'  => '<div class="custom-metabox-headline"><h2>' . __('Sección 1', 'polemospolitic'). '</h2></div>',
                // 'callback' => 'display_warning',
            ),
            array(
                'id'     => 'sect1',
                // Group field
                'type'   => 'group',
                // Clone whole group?
                'clone'  => true,
                // Drag and drop clones to reorder them?
                'sort_clone' => true,
                // Sub-fields
                'fields' => array(
                    array(
                        'name'        => __( 'Título de Sección 1', 'polemospolitic' ),
                        'label_description' => __( 'Ingrese un título para esta sección', 'polemospolitic' ),
                        'id'          => $prefix . 'title_sect1',
                        'type'        => 'text',
                        'class' => 'custom-metabox-class',
                        'clone'       => false,
                        'placeholder' => __( 'Ejemplo: "OPINION Y ANÁLISIS"', 'polemospolitic' ),
                        'size'        => 60
                    ),
                    array(
                        'name'     => __( 'Seleccione Categoría para Sección 1', 'polemospolitic' ),
                        'id'       => $prefix . 'cat_sect1',
                        'type'     => 'select_advanced',
                        // Array of 'value' => 'Label' pairs for select box
                        'options'  => get_custom_category(),
                        // Select multiple values, optional. Default is false.
                        'multiple' => false,
                        'std'         => ' ', // Default value, optional
                        'placeholder' => __( 'Seleccione una Categoría', 'polemospolitic' ),
                    ),
                    array(
                        'name'     => __( 'Seleccione Cantidad de Noticias', 'polemospolitic' ),
                        'id'       => $prefix . 'num_sect1',
                        'type'     => 'number',
                        'min'      => 1,
                        'multiple' => false,
                        'std'       => 1,
                        'placeholder' => __( 'Ingrese Cantidad', 'polemospolitic' ),
                    ),
                ),
            ),
            array(
                'id'   => 'custom_html',
                'type' => 'custom_html',
                'std'  => '<div class="custom-metabox-headline"><h2>' . __('Sección 2', 'polemospolitic'). '</h2></div>',
                // 'callback' => 'display_warning',
            ),
            array(
                'name'        => __( 'Título de Sección 2', 'polemospolitic' ),
                'label_description' => __( 'Ingrese un título para esta sección', 'polemospolitic' ),
                'id'          => $prefix . 'title_sect2',
                'type'        => 'text',
                'class' => 'custom-metabox-class',
                'clone'       => false,
                'placeholder' => __( 'Ejemplo: "OPINION Y ANÁLISIS"', 'polemospolitic' ),
                'size'        => 60
            ),
            array(
                'name'     => __( 'Seleccione Categoría para Sección 2', 'polemospolitic' ),
                'id'       => $prefix . 'cat_sect2',
                'type'     => 'select_advanced',
                // Array of 'value' => 'Label' pairs for select box
                'options'  => get_custom_category(),
                // Select multiple values, optional. Default is false.
                'multiple' => false,
                'std'         => ' ', // Default value, optional
                'placeholder' => __( 'Seleccione una Categoría', 'polemospolitic' ),
            ),
            array(
                'name'     => __( 'Seleccione Cantidad de Noticias', 'polemospolitic' ),
                'id'       => $prefix . 'num_sect2',
                'type'     => 'number',
                'min'      => 1,
                'multiple' => false,
                'std'       => 1,
                'placeholder' => __( 'Ingrese Cantidad', 'polemospolitic' ),
            ),

        )
    );

    $meta_boxes[] = array(
        'id'         => 'nosotros_data',
        'title'      => __( 'Secciones de la Pagina / Información Extra', 'polemospolitic' ),
        'post_types' => array( 'page' ),
        'context'    => 'normal',
        'priority'   => 'high',
        'include' => array(
            // With all conditions below, use this logical operator to combine them. Default is 'OR'. Case insensitive. Optional.
            'relation'        => 'OR',
            'slug'            => array( 'polemos-politic' ),
        ),
        'fields' => array(

            array(
                'id'   => 'custom_html',
                'type' => 'custom_html',
                'std'  => '<div class="custom-metabox-headline"><h2>' . __('Sección de "NUESTRO EQUIPO"', 'polemospolitic'). '</h2></div>',
                // 'callback' => 'display_warning',
            ),
            array(
                'id'     => 'nosotros_group',
                // Group field
                'type'   => 'group',
                // Clone whole group?
                'clone'  => true,
                // Drag and drop clones to reorder them?
                'sort_clone' => true,
                // Sub-fields
                'fields' => array(
                    array(
                        'name'     => __( 'Foto', 'polemospolitic' ),
                        'label_description' => __( 'Recomendado: 400x400 en JPG o PNG', 'polemospolitic' ),
                        'id'       => $prefix . 'pic_member',
                        'type'     => 'image_advanced',
                        'max_file_uploads' => 1
                    ),
                    array(
                        'name'        => __( 'Nombre del Miembro del equipo', 'polemospolitic' ),
                        'label_description' => __( 'Ingrese el nombre del miembro del equipo', 'polemospolitic' ),
                        'id'          => $prefix . 'name_member',
                        'type'        => 'text',
                        'class' => 'custom-metabox-class',
                        'clone'       => false,
                        'placeholder' => __( 'Ejemplo: "PEDRO LUIS PEDROSA"', 'polemospolitic' ),
                        'size'        => 60
                    ),
                    array(
                        'name'     => __( 'Descripción del Miembro del equipo', 'polemospolitic' ),
                        'id'       => $prefix . 'desc_member',
                        'type'     => 'WYSIWYG',
                        // Array of 'value' => 'Label' pairs for select box
                        'std'         => ' ', // Default value, optional
                        'placeholder' => __( 'Ingrese una descripción:', 'polemospolitic' ),
                    ),
                ),
            ),


        )
    );

    $meta_boxes[] = array(
        'id'         => 'service_data',
        'title'      => __( 'Servicios / Información Extra', 'polemospolitic' ),
        'post_types' => array( 'servicios' ),
        'context'    => 'advanced',
        'priority'   => 'high',
        'fields' => array(
            array(
                'id'   => 'custom_html',
                'type' => 'custom_html',
                'std'  => '<div class="custom-metabox-headline"><h2>' . __('Opciones para el Inicio:', 'polemospolitic'). '</h2></div>',
            ),
            array(
                'name'     => __( '¿Este servicio deberá mostrarse en el inicio?', 'polemospolitic' ),
                'label_description' => __( 'Seleccione si el servicio deberá mostrarse en el inicio justo luego del slider', 'polemospolitic' ),
                'id'       => $prefix . 'service_featured',
                'type'     => 'checkbox',
            ),
            array(
                'id'   => 'custom_html',
                'type' => 'custom_html',
                'std'  => '<div class="custom-metabox-headline"><h2>' . __('Información Gráfica:', 'polemospolitic'). '</h2></div>',
            ),
            array(
                'name'     => __( 'Ícono para Servicio', 'polemospolitic' ),
                'label_description' => __( 'El mismo será mostrado en el Home', 'polemospolitic' ),
                'id'       => $prefix . 'service_icon',
                'type'     => 'image_advanced',
                'max_file_uploads' => 1
            ),
            array(
                'id'   => 'custom_html',
                'type' => 'custom_html',
                'std'  => '<div class="custom-metabox-headline"><h2>' . __('Opciones Adicionales:', 'polemospolitic'). '</h2></div>',
            ),
            array(
                'name'     => __( '¿Este servicio deberá tener formulario?', 'polemospolitic' ),
                'label_description' => __( 'Seleccione si el servicio tendra un formulario de recopilación de información', 'polemospolitic' ),
                'id'       => $prefix . 'service_form',
                'type'     => 'checkbox',
            ),
            array(
                'name'        => __( 'Shortcode del Formulario', 'polemospolitic' ),
                'label_description' => __( 'Ingrese el shortcode de Contact Form 7 para el formulario correspondiente', 'polemospolitic' ),
                'id'          => $prefix . 'service_shortcode',
                'type'        => 'text',
                'class' => 'custom-metabox-class',
                'clone'       => false,
                'size'        => 60
            ),

        )
    );

    $meta_boxes[] = array(
        'id'         => 'page_data',
        'title'      => __( 'Información Extra', 'polemospolitic' ),
        'post_types' => array( 'page', 'servicios', 'cursos' ),
        'context'    => 'side',
        'priority'   => 'high',
        'exclude' => array(
            // With all conditions below, use this logical operator to combine them. Default is 'OR'. Case insensitive. Optional.
            'relation'        => 'OR',
            'slug'            => array( 'home', 'inicio' ),
            'template'        => array(  'templates-home.php' ),
        ),
        'fields' => array(
            array(
                'id'   => 'custom_html',
                'type' => 'custom_html',
                'std'  => '<div class="custom-metabox-headline"><h2>' . __('Información Gráfica:', 'polemospolitic'). '</h2></div>',
            ),
            array(
                'name'     => __( 'Banner para Sección', 'polemospolitic' ),
                'label_description' => __( 'Recomendado: 1280x400 en JPG o PNG', 'polemospolitic' ),
                'id'       => $prefix . 'page_banner',
                'type'     => 'image_advanced',
                'max_file_uploads' => 1
            )
        )
    );

    $meta_boxes[] = array(
        'id'         => 'post_data',
        'title'      => __( 'Información Extra', 'polemospolitic' ),
        'post_types' => array( 'post' ),
        'context'    => 'side',
        'priority'   => 'high',
        'fields' => array(
            array(
                'id'   => 'custom_html',
                'type' => 'custom_html',
                'std'  => '<div class="custom-metabox-headline"><h2>' . __('Opciones para Entrada:', 'polemospolitic'). '</h2></div>',
            ),
            array(
                'name'     => __( 'Activar banner tamaño completo', 'polemospolitic' ),
                'label_description' => __( 'Seleccione si el post debe tener un banner grande', 'polemospolitic' ),
                'id'       => $prefix . 'post_check',
                'type'     => 'checkbox',
            ),
            array(
                'name'     => __( 'Banner para Sección', 'polemospolitic' ),
                'label_description' => __( 'Recomendado: 1280x400 en JPG o PNG', 'polemospolitic' ),
                'id'       => $prefix . 'page_banner',
                'type'     => 'image_advanced',
                'max_file_uploads' => 1
            )
        )
    );

     $meta_boxes[] = array(
        'id'         => 'cursos_data',
        'title'      => __( 'Cursos / Información Extra', 'polemospolitic' ),
        'post_types' => array( 'cursos' ),
        'context'    => 'advanced',
        'priority'   => 'high',
        'fields' => array(
            array(
                'id'   => 'custom_html',
                'type' => 'custom_html',
                'std'  => '<div class="custom-metabox-headline"><h2>' . __('Opciones Adicionales:', 'polemospolitic'). '</h2></div>',
            ),
            array(
                'name'     => __( '¿Este servicio deberá tener formulario?', 'polemospolitic' ),
                'label_description' => __( 'Seleccione si el servicio tendra un formulario de recopilación de información', 'polemospolitic' ),
                'id'       => $prefix . 'service_form',
                'type'     => 'checkbox',
            ),
            array(
                'name'        => __( 'Shortcode del Formulario', 'polemospolitic' ),
                'label_description' => __( 'Ingrese el shortcode de Contact Form 7 para el formulario correspondiente', 'polemospolitic' ),
                'id'          => $prefix . 'service_shortcode',
                'type'        => 'text',
                'class' => 'custom-metabox-class',
                'clone'       => false,
                'size'        => 60
            ),

        )
    );


    return $meta_boxes;
}

if ( ! function_exists( 'rwmb_meta' ) ) {
    function rwmb_meta( $key, $args = '', $post_id = null ) {
        return false;
    }
}
