<?php
/** FUNCION PARA COLOCAR TIEMPO EN ENTRADAS **/
function polemospolitic_time_ago() {
    global $post;
    $date = get_post_time('G', true, $post);
    $chunks = array(
        array( 60 * 60 * 24 * 365 , __( 'año', 'polemospolitic' ), __( 'años', 'polemospolitic' ) ),
        array( 60 * 60 * 24 * 30 , __( 'mes', 'polemospolitic' ), __( 'meses', 'polemospolitic' ) ),
        array( 60 * 60 * 24 * 7, __( 'semana', 'polemospolitic' ), __( 'semanas', 'polemospolitic' ) ),
        array( 60 * 60 * 24 , __( 'dia', 'polemospolitic' ), __( 'dias', 'polemospolitic' ) ),
        array( 60 * 60 , __( 'hora', 'polemospolitic' ), __( 'horas', 'polemospolitic' ) ),
        array( 60 , __( 'minuto', 'polemospolitic' ), __( 'minutos', 'polemospolitic' ) ),
        array( 1, __( 'segundo', 'polemospolitic' ), __( 'segundos', 'polemospolitic' ) )
    );
    if ( !is_numeric( $date ) ) {
        $time_chunks = explode( ':', str_replace( ' ', ':', $date ) );
        $date_chunks = explode( '-', str_replace( ' ', '-', $date ) );
        $date = gmmktime( (int)$time_chunks[1], (int)$time_chunks[2], (int)$time_chunks[3], (int)$date_chunks[1], (int)$date_chunks[2], (int)$date_chunks[0] );
    }
    $current_time = current_time( 'mysql', $gmt = 0 );
    $newer_date = time( );
    $since = $newer_date - $date;
    if ( 0 > $since )
        return __(  ' un momento', 'polemospolitic' );
    for ( $i = 0, $j = count($chunks); $i < $j; $i++) {
        $seconds = $chunks[$i][0];
        if ( ( $count = floor($since / $seconds) ) != 0 )
            break;
    }
    $output = ( 1 == $count ) ? '1 '. $chunks[$i][1] : $count . ' ' . $chunks[$i][2];
    if ( !(int)trim($output) ){
        $output = '0 ' . __( 'segundos', 'polemospolitic' );
    }
    return $output;
}

/* CUSTOM EXCERPT */
function get_excerpt($count){
    global $wp_query;
    $foto = 0;
    $permalink = get_permalink($post->ID);
    $category = get_taxonomies($post->ID);
    $excerpt = get_the_excerpt($post->ID);
    if ($excerpt == ""){
        $excerpt = get_the_content($post->ID);
    }
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, $count);
    $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
    $excerpt = $excerpt.'... <a class="plus" href="'.$permalink.'">+</a>';
    return $excerpt;
}

function get_excerpt_meta($excerpt){
    $excerpt = strip_tags($excerpt);
    $excerpt = substr($excerpt, 0, 170);
    return $excerpt;
}

/* BREADCRUMBS */
function the_breadcrumb() {
    echo '<ol class="breadcrumb">';
    if (!is_home()) {
        echo '<li><a href="' . home_url('/') . '">'. __( 'Inicio', 'polemospolitic' ) . '</a></li>';
        if (is_category()) {
            echo '<li>' . get_the_category('title_li=') . '</li>';
            if (is_single()) {
                echo '<li class="active">' . get_the_title() . '</li>';
            }
        }

        if (is_archive('servicios')) {
            echo '<li class="active">' . __( 'Servicios', 'polemospolitic' ) . '</li>';
        }
        if (is_singular('servicios')) {
            echo '<li><a href="' . home_url('servicios/') . '">'. __( 'Servicios', 'polemospolitic' ) . '</a></li>';
            echo '<li class="active">' . get_the_title() . '</li>';
        }
        if (is_singular('post')) {
            echo '<li><a href="' . home_url('temas-de-interes/') . '">'. __( 'Temas de Interés', 'polemospolitic' ) . '</a></li>';
            echo '<li class="active">' . get_the_title() . '</li>';
        }
        if (is_page()) {
            echo '<li class="active">' . get_the_title() . '</li>';
        }
        if (is_singular('cursos')) {
            echo '<li><a href="' . home_url('/servicios/formacion/') . '">'. __( 'Capacitación y Formación', 'polemospolitic' ) . '</a></li>';
            echo '<li class="active">' . get_the_title() . '</li>';
        }
    }
    if ( !is_front_page() && is_home() ) {
        echo '<li><a href="' . home_url('/') . '">'. __( 'Inicio', 'polemospolitic' ) . '</a></li>';
        echo '<li class="active">' . __( 'Temas de Interés', 'polemospolitic' ) . '</li>';
    }
    echo '</ol>';
}

/* IMAGES RESPONSIVE ON ATTACHMENT IMAGES */
function image_tag_class($class) {
    $class .= ' img-responsive';
    return $class;
}
add_filter('get_image_tag_class', 'image_tag_class' );

/* ADD CONTENT WIDTH FUNCTION */

if ( ! isset( $content_width ) ) $content_width = 1170;

function get_custom_metadata_info ($current_url, $custom_type) {
    if ($custom_type == 'description') {
        $args = array('name' => $current_url, 'posts_per_page' => 1);
        $my_posts = get_posts( $args );
        if( $my_posts ) {
            $excerpt = $my_posts[0]->post_excerpt;
            if ( !$excerpt ) {
                $excerpt = get_excerpt_meta($my_posts[0]->post_content);
            }
            return '<meta name="description" content="' . htmlentities($excerpt, ENT_QUOTES, 'UTF-8') . '" />';
        }
    }
    if ($custom_type == 'og:description') {
        $args = array('name' => $current_url, 'posts_per_page' => 1);
        $my_posts = get_posts( $args );
        if( $my_posts ) {
            $excerpt = $my_posts[0]->post_excerpt;
            if ( !$excerpt ) {
                $excerpt = get_excerpt_meta($my_posts[0]->post_content);
            }
            return '<meta property="og:description" content="' . htmlentities($excerpt, ENT_QUOTES, 'UTF-8') . '" />';
        }
    }
    if ($custom_type == 'keywords') {
        $args = array('name' => $current_url, 'posts_per_page' => 1);
        $my_posts = get_posts( $args );
        if( $my_posts ) {
            $terms = get_the_terms( $my_posts[0]->ID, 'post_tag' );
            $on_draught = '';
            if ( $terms && !is_wp_error( $terms ) ) {
                $draught_links = array();
                foreach ( $terms as $term ) {
                    $draught_links[] = $term->name;
                }
            } else {
                $cats = get_the_category( $my_posts[0]->ID, 'category' );
                if ( $cats && !is_wp_error( $cats ) ) :
                $draught_links = array();
                foreach ( $cats as $term ) {
                    $draught_links[] = $term->name;
                }
                endif;
            }
            $on_draught = join( ", ", $draught_links );
            return '<meta name="keywords" content="Polemos Politic, consultor politico, consultor empresarial, coaching estratégico, formación profesional, organización política, '. $on_draught .'" />';
        }
    }
}
?>
