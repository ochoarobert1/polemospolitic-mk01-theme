<?php

function servicios() {

    $labels = array(
        'name'                  => _x( 'Servicios', 'Post Type General Name', 'polemospolitic' ),
        'singular_name'         => _x( 'Servicio', 'Post Type Singular Name', 'polemospolitic' ),
        'menu_name'             => __( 'Servicios', 'polemospolitic' ),
        'name_admin_bar'        => __( 'Servicios', 'polemospolitic' ),
        'archives'              => __( 'Archivo de Servicios', 'polemospolitic' ),
        'attributes'            => __( 'Atributos de Servicio', 'polemospolitic' ),
        'parent_item_colon'     => __( 'Servicio Padre:', 'polemospolitic' ),
        'all_items'             => __( 'Todos los Servicios', 'polemospolitic' ),
        'add_new_item'          => __( 'Agregar Nuevo Servicio', 'polemospolitic' ),
        'add_new'               => __( 'Agregar Nuevo', 'polemospolitic' ),
        'new_item'              => __( 'Nuevo Servicio', 'polemospolitic' ),
        'edit_item'             => __( 'Editar Servicio', 'polemospolitic' ),
        'update_item'           => __( 'Actualizar Servicio', 'polemospolitic' ),
        'view_item'             => __( 'Ver Servicio', 'polemospolitic' ),
        'view_items'            => __( 'Ver Servicios', 'polemospolitic' ),
        'search_items'          => __( 'Buscar Servicio', 'polemospolitic' ),
        'not_found'             => __( 'No hay resultados', 'polemospolitic' ),
        'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'polemospolitic' ),
        'featured_image'        => __( 'Imagen Descriptiva', 'polemospolitic' ),
        'set_featured_image'    => __( 'Colocar Imagen Descriptiva', 'polemospolitic' ),
        'remove_featured_image' => __( 'Remover Imagen Descriptiva', 'polemospolitic' ),
        'use_featured_image'    => __( 'Usar como Imagen Descriptiva', 'polemospolitic' ),
        'insert_into_item'      => __( 'Insertar dentro de Servicio', 'polemospolitic' ),
        'uploaded_to_this_item' => __( 'Cargado a este Servicio', 'polemospolitic' ),
        'items_list'            => __( 'Listado de Servicios', 'polemospolitic' ),
        'items_list_navigation' => __( 'Navegación de Listado de Servicios', 'polemospolitic' ),
        'filter_items_list'     => __( 'Filtro de Listado de Servicios', 'polemospolitic' ),
    );
    $args = array(
        'label'                 => __( 'Servicio', 'polemospolitic' ),
        'description'           => __( 'Servicios dentro de la empresa', 'polemospolitic' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 7,
        'menu_icon'             => 'dashicons-screenoptions',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
        'show_in_rest'          => true,
    );
    register_post_type( 'servicios', $args );

}
add_action( 'init', 'servicios', 0 );

function multimedia() {

    $labels = array(
        'name'                  => _x( 'Multimedia', 'Post Type General Name', 'polemospolitic' ),
        'singular_name'         => _x( 'Multimedia', 'Post Type Singular Name', 'polemospolitic' ),
        'menu_name'             => __( 'Audio / Video', 'polemospolitic' ),
        'name_admin_bar'        => __( 'Audio / Video', 'polemospolitic' ),
        'archives'              => __( 'Archivo de Multimedia', 'polemospolitic' ),
        'attributes'            => __( 'Atributos de Multimedia', 'polemospolitic' ),
        'parent_item_colon'     => __( 'Multimedia Padre:', 'polemospolitic' ),
        'all_items'             => __( 'Todos los Multimedias', 'polemospolitic' ),
        'add_new_item'          => __( 'Agregar Nuevo Multimedia', 'polemospolitic' ),
        'add_new'               => __( 'Agregar Nuevo', 'polemospolitic' ),
        'new_item'              => __( 'Nuevo Multimedia', 'polemospolitic' ),
        'edit_item'             => __( 'Editar Multimedia', 'polemospolitic' ),
        'update_item'           => __( 'Actualizar Multimedia', 'polemospolitic' ),
        'view_item'             => __( 'Ver Multimedia', 'polemospolitic' ),
        'view_items'            => __( 'Ver Multimedias', 'polemospolitic' ),
        'search_items'          => __( 'Buscar Multimedia', 'polemospolitic' ),
        'not_found'             => __( 'No hay resultados', 'polemospolitic' ),
        'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'polemospolitic' ),
        'featured_image'        => __( 'Imagen Descriptiva', 'polemospolitic' ),
        'set_featured_image'    => __( 'Colocar Imagen Descriptiva', 'polemospolitic' ),
        'remove_featured_image' => __( 'Remover Imagen Descriptiva', 'polemospolitic' ),
        'use_featured_image'    => __( 'Usar como Imagen Descriptiva', 'polemospolitic' ),
        'insert_into_item'      => __( 'Insertar dentro de Multimedia', 'polemospolitic' ),
        'uploaded_to_this_item' => __( 'Cargado a este Multimedia', 'polemospolitic' ),
        'items_list'            => __( 'Listado de Multimedia', 'polemospolitic' ),
        'items_list_navigation' => __( 'Navegación de Listado de Multimedia', 'polemospolitic' ),
        'filter_items_list'     => __( 'Filtro de Listado de Multimedia', 'polemospolitic' ),
    );
    $args = array(
        'label'                 => __( 'Multimedia', 'polemospolitic' ),
        'description'           => __( 'Videos / Entrevistas / Otros', 'polemospolitic' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
        'taxonomies'            => array( 'multimedia_tag' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 8,
        'menu_icon'             => 'dashicons-video-alt3',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
        'show_in_rest'          => true,
    );
    register_post_type( 'multimedia', $args );

}
add_action( 'init', 'multimedia', 0 );

// Register Custom Taxonomy
function multimedia_tag() {

    $labels = array(
        'name'                       => _x( 'Etiquetas', 'Taxonomy General Name', 'polemospolitic' ),
        'singular_name'              => _x( 'Etiqueta', 'Taxonomy Singular Name', 'polemospolitic' ),
        'menu_name'                  => __( 'Etiquetas', 'polemospolitic' ),
        'all_items'                  => __( 'Todas las Etiquetas', 'polemospolitic' ),
        'parent_item'                => __( 'Etiqueta Padre', 'polemospolitic' ),
        'parent_item_colon'          => __( 'Etiqueta Padre:', 'polemospolitic' ),
        'new_item_name'              => __( 'Nueva Etiqueta', 'polemospolitic' ),
        'add_new_item'               => __( 'Agregar Nueva Etiqueta', 'polemospolitic' ),
        'edit_item'                  => __( 'Editar Etiqueta', 'polemospolitic' ),
        'update_item'                => __( 'Actualizar Etiqueta', 'polemospolitic' ),
        'view_item'                  => __( 'Ver Etiqueta', 'polemospolitic' ),
        'separate_items_with_commas' => __( 'Separar Etiquetas por comas', 'polemospolitic' ),
        'add_or_remove_items'        => __( 'Agregar o remover Etiquetas', 'polemospolitic' ),
        'choose_from_most_used'      => __( 'Escoger de las más usadas', 'polemospolitic' ),
        'popular_items'              => __( 'Etiquetas Populares', 'polemospolitic' ),
        'search_items'               => __( 'Buscar Etiquetas', 'polemospolitic' ),
        'not_found'                  => __( 'No hay resultados', 'polemospolitic' ),
        'no_terms'                   => __( 'No hay Etiquetas', 'polemospolitic' ),
        'items_list'                 => __( 'Listado de Etiquetas', 'polemospolitic' ),
        'items_list_navigation'      => __( 'Navegación del Listado de Etiquetas', 'polemospolitic' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
        'show_in_rest'               => true,
    );
    register_taxonomy( 'multimedia_tag', array( 'multimedia' ), $args );

}
add_action( 'init', 'multimedia_tag', 0 );

// Register Custom Post Type
function cursos() {

    $labels = array(
        'name'                  => _x( 'Cursos', 'Post Type General Name', 'polemospolitic' ),
        'singular_name'         => _x( 'Curso', 'Post Type Singular Name', 'polemospolitic' ),
        'menu_name'             => __( 'Cursos', 'polemospolitic' ),
        'name_admin_bar'        => __( 'Cursos', 'polemospolitic' ),
        'archives'              => __( 'Archivo de Cursos', 'polemospolitic' ),
        'attributes'            => __( 'Atributos de Cursos', 'polemospolitic' ),
        'parent_item_colon'     => __( 'Curso Padre:', 'polemospolitic' ),
        'all_items'             => __( 'Todos los Cursos', 'polemospolitic' ),
        'add_new_item'          => __( 'Agregar nuevo Curso', 'polemospolitic' ),
        'add_new'               => __( 'Agregar Nuevo', 'polemospolitic' ),
        'new_item'              => __( 'Nuevo Curso', 'polemospolitic' ),
        'edit_item'             => __( 'Editar Curso', 'polemospolitic' ),
        'update_item'           => __( 'Actualizar Curso', 'polemospolitic' ),
        'view_item'             => __( 'Ver Curso', 'polemospolitic' ),
        'view_items'            => __( 'Ver Cursos', 'polemospolitic' ),
        'search_items'          => __( 'Buscar Curso', 'polemospolitic' ),
        'not_found'             => __( 'No hay resultados', 'polemospolitic' ),
        'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'polemospolitic' ),
        'featured_image'        => __( 'Imagen del Curso', 'polemospolitic' ),
        'set_featured_image'    => __( 'Colocar Imagen del Curso', 'polemospolitic' ),
        'remove_featured_image' => __( 'Remover Imagen del Curso', 'polemospolitic' ),
        'use_featured_image'    => __( 'Usar como Imagen del Curso', 'polemospolitic' ),
        'insert_into_item'      => __( 'Insertar en Curso', 'polemospolitic' ),
        'uploaded_to_this_item' => __( 'Cargado a este Curso', 'polemospolitic' ),
        'items_list'            => __( 'Listado de Curso', 'polemospolitic' ),
        'items_list_navigation' => __( 'Navegación del Listado de Curso', 'polemospolitic' ),
        'filter_items_list'     => __( 'Filtro del Listado de Curso', 'polemospolitic' ),
    );
    $args = array(
        'label'                 => __( 'Curso', 'polemospolitic' ),
        'description'           => __( 'Cursos y Diplomados ofrecidos por Polemos Politic', 'polemospolitic' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor', 'excerpt', 'thumbnail', ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 9,
        'menu_icon'             => 'dashicons-welcome-learn-more',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => true,
        'exclude_from_search'   => false,
        'publicly_queryable'    => true,
        'capability_type'       => 'post',
    );
    register_post_type( 'cursos', $args );

}
add_action( 'init', 'cursos', 0 );

add_action( 'admin_init', 'add_admin_menu_separator' );


function add_admin_menu_separator( $position ) {

    global $menu;

    $menu[ $position ] = array(
        0    =>    '',
        1    =>    'read',
        2    =>    'separator' . $position,
        3    =>    '',
        4    =>    'wp-menu-separator'
    );

}

add_action( 'admin_menu', 'set_admin_menu_separator' );

function set_admin_menu_separator() {  do_action( 'admin_init', 6 );  do_action( 'admin_init', 30 );  }
