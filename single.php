<?php get_header(); ?>
<?php the_post(); ?>
<?php $template = get_post_meta(get_the_ID(), 'rw_post_check', true); ?>
<?php if ($template == 1) { ?>
<main class="container-fluid">
    <div class="row">
        <?php $images = rwmb_meta( 'rw_page_banner', 'size=full' );  ?>
        <?php if ( !empty( $images ) ) { ?>
        <?php foreach ( $images as $image ) { $full_url = $image['full_url']; } ?>
        <?php } ?>
        <div class="page-banner col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr animated fadeIn" style="background: url(<?php echo $full_url; ?>);">
            <div class="page-banner-wrapper"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 itemprop="headline"><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
            <div class="the-breadcrumbs col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php echo the_breadcrumb(); ?>
            </div>
        </div>
    </div>
</main>
<main class="container">
    <div class="row">
        <div class="single-main-container col-lg-12 col-md-12 col-sm-12 col-xs-12" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
            <?php $defaultargs = array('class' => 'img-responsive'); ?>
            <article id="post-<?php the_ID(); ?>" class="the-single col-lg-9 col-md-9 col-sm-9 col-xs-12 <?php echo join(' ', get_post_class()); ?>" itemscope itemtype="http://schema.org/Article">
                <header>
                    <?php the_tags( __( 'Tags: ', 'polemospolitic' ), ', ', '<br>'); // Separated by commas with a line break at the end ?>
                </header>
                <div class="post-content" itemprop="articleBody">
                    <?php the_content() ?>
                    <?php wp_link_pages( array(
    'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'polemospolitic' ) . '</span>',
    'after'       => '</div>',
    'link_before' => '<span>',
    'link_after'  => '</span>', ) ); ?>
                    <footer>
                        <p>Categorias: <?php the_category(', '); // Separated by commas ?></p>
                        <span class="date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span>
                        <span class="author">Publicado por: <?php the_author_posts_link(); ?></span>
                    </footer>
                </div><!-- .post-content -->
                <meta itemprop="datePublished" datetime="<?php echo get_the_time('Y-m-d') ?>" content="<?php echo get_the_date('i') ?>">
                <meta itemprop="author" content="<?php echo esc_attr(get_the_author()) ?>">
                <meta itemprop="url" content="<?php the_permalink() ?>">
                <?php if ( comments_open() ) { comments_template(); } ?>
            </article> <?php // end article ?>

            <aside class="the-sidebar col-lg-3 col-md-3 col-sm-3 hidden-xs" role="complementary">
                <?php get_sidebar(); ?>
            </aside>
        </div>
    </div>
</main>

<?php } else { ?>
<main class="container">
    <div class="row">
        <div class="single-main-container col-lg-12 col-md-12 col-sm-12 col-xs-12" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

            <?php /* POST FORMAT - DEFAULT */ ?>
            <?php $defaultargs = array('class' => 'img-responsive'); ?>
            <article id="post-<?php the_ID(); ?>" class="the-single col-lg-9 col-md-9 col-sm-9 col-xs-12 <?php echo join(' ', get_post_class()); ?>" itemscope itemtype="http://schema.org/Article">
                <header>
                    <div class="single-categories"><?php the_category(' '); // Separated by commas ?></div>
                    <h1 itemprop="name"><?php the_title(); ?></h1>
                    <div class="date"><?php the_time('F j, Y'); ?></div>

                </header>
                <?php if ( has_post_thumbnail()) : ?>
                <picture>
                    <?php the_post_thumbnail('single_img', $defaultargs); ?>
                </picture>
                <?php endif; ?>
                <div class="post-content" itemprop="articleBody">
                    <?php the_content() ?>
                    <?php wp_link_pages( array(
    'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'polemospolitic' ) . '</span>',
    'after'       => '</div>',
    'link_before' => '<span>',
    'link_after'  => '</span>', ) ); ?>
                    <footer>
                        <?php the_tags( __( 'Tags: ', 'polemospolitic' ), ', ', '<br>'); // Separated by commas with a line break at the end ?>
                    </footer>
                </div><!-- .post-content -->
                <meta itemprop="datePublished" datetime="<?php echo get_the_time('Y-m-d') ?>" content="<?php echo get_the_date('i') ?>">
                <meta itemprop="author" content="<?php echo esc_attr(get_the_author()) ?>">
                <meta itemprop="url" content="<?php the_permalink() ?>">
                <?php if ( comments_open() ) { comments_template(); } ?>
            </article> <?php // end article ?>

            <aside class="the-sidebar col-lg-3 col-md-3 col-sm-3 hidden-xs" role="complementary">
                <?php get_sidebar(); ?>
            </aside>
        </div>
    </div>
</main>
<?php } ?>

<?php get_footer(); ?>
