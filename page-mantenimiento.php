<?php get_header('mantenimiento'); ?>
<?php the_post(); ?>
<main class="container" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="page-container col-lg-12 col-md-12 col-sm-12 col-xs-12" role="article" itemscope itemtype="http://schema.org/BlogPosting">
            <article id="post-<?php the_ID(); ?>" class="page-content <?php echo join(' ', get_post_class()); ?>" >
                <div class="page-article col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr" itemprop="articleBody">
                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.png" alt="<?php echo bloginfo('name'); ?>" class="img-responsive img-mantenimiento animated fadeIn delay-1" />
                    <div class="animated fadeIn delay-2">
                        <?php the_content(); ?>
                    </div>
                </div>
            </article>
        </section>
    </div>
</main>
<?php get_footer('mantenimiento'); ?>
