<?php get_header(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <?php $images = rwmb_meta( 'rw_page_banner', 'size=full' );  ?>
        <?php if ( !empty( $images ) ) { ?>
        <?php foreach ( $images as $image ) { $full_url = $image['full_url']; } ?>
        <?php } ?>
        <div class="page-banner col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr animated fadeIn" style="background: url(<?php echo $full_url; ?>);">
            <div class="page-banner-wrapper"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 itemprop="headline"><?php _e('Nuestros Servicios', 'polemospolitic') ?></h1>
                    </div>
                </div>
            </div>
            <div class="the-breadcrumbs col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php echo the_breadcrumb(); ?>
            </div>
        </div>
        <section class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="container">
                <div class="row">
                    <div class="archive-services-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h3>Un vistazo a los Servicios principales</h3>
                        <?php $defaultatts = array('class' => 'img-responsive'); ?>
                        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                        <article id="post-<?php the_ID(); ?>" class="archive-item archive-servicios-item col-lg-6 col-md-6 col-sm-6 col-xs-12 no-paddingl no-paddingr <?php echo join(' ', get_post_class()); ?>" role="article">
                            <picture class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                    <?php if ( function_exists( 'rwmb_meta' ) ) { ?>
                                    <?php $icons = rwmb_meta( 'rw_service_icon', 'size=full' ); ?>
                                    <?php if ( !empty( $icons ) ) { ?>
                                    <?php foreach ( $icons as $image ) { ?>
                                    <img src="<?php echo esc_url( $image['url'] ); ?>" title="<?php echo get_the_title(); ?>" alt="<?php echo esc_attr( $image['alt'] ); ?>" class="img-responsive service-icon" />
                                    <?php } } } ?>
                                </a>
                            </picture>
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><h2 rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></h2></a>
                                <p><?php the_excerpt(); ?></p>
                            </div>
                            <div class="clearfix"></div>
                        </article>
                        <?php endwhile; ?>
                        <div class="pagination col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <?php if(function_exists('wp_paginate')) { wp_paginate(); } else { posts_nav_link(); wp_link_pages(); } ?>
                        </div>
                    </div>
                    <?php else: ?>
                    <article>
                        <h2><?php _e('Disculpe, su busqueda no arrojo ningun resultado', 'polemospolitic'); ?></h2>
                        <h3><?php _e('Dirígete nuevamente al', 'polemospolitic'); ?> <a href="<?php echo home_url('/'); ?>" title="<?php _e('Volver al Inicio', 'polemospolitic'); ?>"><?php _e('inicio', 'polemospolitic'); ?></a>.</h3>
                    </article>
                    <?php endif; ?>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
