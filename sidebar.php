<?php if ( is_active_sidebar( 'main_sidebar' ) ) : ?>
<ul id="sidebar">
    <li>
        <h2 class="widgettitle">Destacados</h2>
        <div class="media">
            <div class="media-left">
                <a href="#">
                    <img class="media-object" src="http://www.polemospolitic.com/wp-content/uploads/2016/11/asi-se-exporto-la-revolucion.jpg" alt="<?php echo get_bloginfo('name'); ?>" />
                </a>
            </div>
            <div class="media-body">
                <h4 class="media-heading">ASÍ SE EXPORTO LA REVOLUCIÓN</h4>
                <p>Pedro Pedrosa</p>
                <p>Esta obra es una nueva manera de abordar la penetracion del Socialismo del Siglo XXI, sus alcances, los riesgos que implica para los países libres y su proterva evolución. </p>
            </div>
        </div>
    </li>
    <?php dynamic_sidebar( 'main_sidebar' ); ?>
</ul>
<?php endif; ?>
