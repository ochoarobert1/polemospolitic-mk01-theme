<a href="http://www.polemospolitic.com/wp-content/uploads/2013/11/Logo_UCV_transp.png"><img class="size-full wp-image-3465 alignleft" style="margin-top: 15px; margin-bottom: 15px;" src="http://www.polemospolitic.com/wp-content/uploads/2013/11/Logo_UCV_transp.png" alt="Logo_UCV_transp" width="120" height="120" /></a>[caption id="attachment_3444" align="alignright" width="120"]<a href="http://www.cursosucv.com"><img class=" wp-image-3444 " src="http://www.polemospolitic.com/wp-content/uploads/2013/11/LOGOCEP-150x150.jpg" alt="CEP - FACES" width="120" height="120" /></a> HAGA CLICK SOBRE LA IMAGEN[/caption]
<h4 style="text-align: justify;">NUESTROS PROGRAMAS ESTAN AVALADOS POR EL CENTRO DE EXTENSIÓN PROFESIONAL DE LA FACULTAD DE CIENCIAS ECONÓMICAS Y SOCIALES DE LA UNIVERSIDAD CENTRAL DE VENEZUELA (CEP FACES UCV)</h4>
<div class="clearfix"></div>
[accordion title="a. Inteligencias Múltiples y Toma de Decisiones"]
<p style="text-align: justify;">Este seminario aborda el análisis de la toma de decisiones con base en una variable hasta ahora poco considerada: la inteligencia humana. Desde una óptica netamente interdisciplinaria y tomando en cuenta los resultados de las más recientes investigaciones neuro científicas, el programa se centra en el estudio de la Teoría de las Inteligencias Múltiples inicialmente desarrollada por Howard Gardner y los diferentes modelos desarrollados a partir de ésta, con la finalidad de establecer cómo cada una de las diversas formas de inteligencia influye y puede ser empleada al momento de elaborar decisiones.</p>
<strong>a.1  Objetivos Generales:</strong>

Al finalizar el Seminario, el participante estará en capacidad de:
<ul>
 	<li style="text-align: justify;">Establecer cuáles han sido las principales variables de estudio incorporadas hasta la fecha en las Teorías de la Toma de Decisiones.</li>
 	<li style="text-align: justify;">Comprender cómo las investigaciones desarrolladas en el marco de la Neurociencia han originado una transformación en el uso del concepto de Inteligencia.</li>
 	<li style="text-align: justify;">Comprender la Teoría de las Inteligencias Múltiples desarrollada por Howard Gardner y las diferentes clasificaciones de las inteligencias desarrolladas a partir de aquélla.</li>
 	<li style="text-align: justify;">Entender cómo las diversas formas de inteligencia pueden incidir en un proceso de toma de decisiones en el ámbito político.</li>
 	<li style="text-align: justify;">Aplicar las diferentes formas de inteligencia a la toma de decisiones frente a problemas reales en el mundo político.</li>
</ul>
<strong>a.2  Contenidos Programáticos:</strong>

&nbsp;
<ul>
 	<li style="text-align: justify;">Las variables históricas en el estudio de la Toma de Decisiones. Principales modelos y teorías.</li>
 	<li style="text-align: justify;">La noción de Inteligencia: evolución histórica e impacto en el estudio del comportamiento humano. Tendencias recientes a la luz de los hallazgos de la Neurociencia.</li>
 	<li style="text-align: justify;">Teoría de las Inteligencias Múltiples de Howard Gardner: un nuevo enfoque acerca de la Inteligencia Humana.</li>
 	<li style="text-align: justify;">Los distintos Modelos y clasificaciones de las Inteligencias Múltiples y sus implicaciones en el estudio y comprensión de los procesos de toma de decisiones.</li>
 	<li style="text-align: justify;">Casos de estudio y aplicaciones prácticas.</li>
</ul>
<strong>a.3  Facilitador:</strong>
<ul>
 	<li>Dra. Maria Elena Pinto (Profesora de Negociación UCV).</li>
</ul>
<strong> a.4  Duración:</strong>
<ul>
 	<li>Dieciséis (16) horas académicas.</li>
</ul>
[/accordion]

[accordion title="b. Transformación de conflictos y neurociencia:"]
<p style="text-align: justify;">Este Seminario se enmarca dentro de la más reciente vertiente de estudios dentro del Manejo de Conflictos, la <i>Transformación de Conflictos</i>, la cual no solo se nutre de dos que le preceden, sino que además proporciona una perspectiva sistémica y englobadora para el estudio y el abordaje de los conflictos que le confiere gran solidez epistemológica y utilidad práctica. La perspectiva transformadora es complementada en este Seminario con los más recientes aportes de la Neurociencia, ciencia transdisciplinaria que nos ha permitido tener un mejor conocimiento de cómo funciona el Cerebro y cómo esto incide en nuestras conductas, en particular, en el marco de situaciones altamente exigentes a nivel cognitivo y afectivo como lo son los conflictos.</p>
<strong>b.1  Objetivos Generales:</strong>

Al finalizar el Seminario, el participante estará en capacidad de:
<ul>
 	<li style="text-align: justify;">Entender el conflicto como proceso dentro del mundo político-social, su naturaleza, dinámica de desarrollo y elementos constitutivos.</li>
 	<li style="text-align: justify;">Establecer las diferencias entre las tres grandes corrientes de estudio dentro de la sub disciplina del Manejo de Conflictos.</li>
 	<li style="text-align: justify;">Comprender los postulados teórico-prácticos de la Transformación de conflictos en tanto teoría y praxis para el abordaje de los conflictos.</li>
 	<li style="text-align: justify;"> Comprender los aportes de la Neurociencia al estudio del conflicto en el ámbito político-social.</li>
 	<li style="text-align: justify;">Emplear herramientas analíticas y prácticas para el estudio y abordaje de conflictos en el ámbito político-social.</li>
</ul>
&nbsp;

<strong>b.2  Contenidos Programáticos:</strong>

&nbsp;
<ul>
 	<li>Definición de Conflicto. Conflicto y Política.</li>
 	<li>El ciclo del conflicto: latencia, conflicto abierto, lucha, umbral de violencia.</li>
 	<li>Formas de abordaje: arreglo, resolución, transformación.</li>
 	<li>Elementos del conflicto: problemas, intereses &amp; necesidades, actores, percepciones, comunicación, relación, alternativas y opciones.</li>
 	<li>El conflicto analizado desde el “Modelo del Cerebro Triuno de Paul</li>
 	<li>Mac Lean: Cerebro Básico (“supervivencia” y necesidades), Cerebro Límbico (emociones, motivaciones y relaciones), Neo corteza (análisis causa-efectos-síntomas, pensamiento lateral y formulación de opciones).</li>
 	<li>La transformación del conflicto como proceso. Niveles de la transformación: personal, interpersonal, estructural, “cultural”.</li>
 	<li>Técnicas de comunicación para la transformación del conflicto.</li>
 	<li>Técnicas para formular opciones de acuerdos basadas en el “Modelo del Cerebro Triuno”</li>
</ul>
&nbsp;

<strong>b.3  Facilitador:</strong>
<ul>
 	<li>Dra. Maria Elena Pinto (Profesora de Negociación UCV).</li>
</ul>
<strong>b.4  Duración:</strong>
<ul>
 	<li>Dieciséis (16) horas académicas.</li>
</ul>
[/accordion]

[accordion title="c. Fundamentos de SPSS (Statistical Pakage for the Social Sciences):"]
<p style="text-align: justify;">Este programa pretende transmitir al cursante los principios fundamentales para la utilización de esta herramienta informática, cuyo uso permite trabajar con grandes amplias bases de datos y una sencilla interface para realizar la mayoría de los análisis. Su empleo como herramienta de medición de opinión pública ha crecido exitosamente, más allá de sus virtudes como herramienta para el estudio de fenómenos de mercado.</p>
<strong>c.1  Objetivos Generales:</strong>

Al finalizar este curso, el participante estará en capacidad de:
<ul>
 	<li>Entender la importancia de contar con SPSS para la realización de encuestas y estudios de opinión pública.</li>
 	<li>Conocer y manejar los módulos que componen el sistema.</li>
 	<li>Crear variables en el sistema vinculadas al análisis de opinión pública.</li>
 	<li>Dominar las técnicas para programar la herramienta en función de obtener los cruces entre las variables creadas.</li>
</ul>
<strong>c.2  Contenidos Programáticos:</strong>
<ul>
 	<li>Definición y Finalidad de SPSS.</li>
 	<li> Módulos que componen el SPSS.</li>
 	<li>Fichero de datos del SPSS.</li>
 	<li>Como operar la interacción entre modulos.</li>
 	<li>Creación de variables.</li>
 	<li>Manejo de bases de datos.</li>
</ul>
<strong>c.3  Facilitador:</strong>
<ul>
 	<li>Dr. José Vicente Carrasquero (Profesor de opinión pública en la Universidad Católica Andrés Bello).</li>
</ul>
<strong>c.4  Duración:</strong>
<ul>
 	<li>Ocho (08) horas académicas.</li>
</ul>
[/accordion]

[accordion title="d. Principios Éticos y Legales para la Libertad (ON LINE)"]

Este seminario estudia los diversos aspectos de la libertad desde la perspectiva del liberalismo clásico, presentando una visión poco conocida en Venezuela país que ha sido gobernada desde la perspectiva de la izquierda o centro izquierda y donde los partidos políticos no se presentaron como una alternativa ideológica a sus partidos opositores. Una visión distinta del papel del Estado ante los ciudadanos y del ciudadano ante el Estado, así como un concepto diferente del capitalismo son planteados en este seminario de forma valiente y objetiva.
 
d.1 Objetivos Generales:
 
		 Al finalizar el curso, el participante estará en capacidad de:
ü    Señalar las diferencias entre la visión socialista de la libertad y la visión liberal.
ü    Identificar los derechos del hombre desde la perspectiva ius naturalista.
ü    Relacionar la vinculación entre los principios de la moral objetiva y los postulados del liberalismo clásico.
ü    Relacionar los orígenes de la civilización occidental con los principios éticos del liberalismo.
 
d.2	Contenidos Programáticos
 
		Tema I. Introducción a la Legislación (Sesiones 1, 2 y 3)
 
ü  Teoría de los Orígenes Legales.
ü  Bases de la civilización occidental judeo-cristiana.
ü  Orígenes de las constituciones.
ü  Derechos del hombre.
ü  Naturaleza del Estado.
ü  Estado de Derecho vs Estado de Justicia.
ü  Leyes Malas.
ü  Estructura Legal de Venezuela.
ü  República versus democracia.
 
		Tema II. Ética y Principios (Sesiones 4 y 5)
 
ü          Principios éticos de la civilización occidental.
ü          Derechos del hombre y su ética.
ü          Naturaleza del estado.
ü          Principios Morales objetivos.
ü          Efectos distorsionadores del estado "social".
ü          El atavismo de la justicia social.
ü          Medios ético-jurídicos que garantizan la sociedad libre.
 
 
 
 
		Tema III. Ética y legislación aplicada (Sesiones 6, 7 y 8) 
 
ü  Ética aplicada a los negocios.
ü  Incentivos vs. Normas morales objetivas.
ü  Medios ético-jurídicos que garantizan la sociedad libre.
ü  Crítica ética a la Responsabilidad Social Empresarial.
ü  Crítica ética al Estado de Bienestar.
ü  Ética de la propiedad privada.
ü  Ética del libre comercio.
ü  ¿Es el socialismo un error intelectual? El socialismo como cualquier sistema de coacción sistemática sobre la función empresarial 
d.3  Facilitador:
	  Dr. Osmel Brito Bigot (Director de la Organización por la Democracia Liberal en Venezuela).
d.4  Duración:
	  Dieciséis (16) horas académicas. Distribuidas en ocho (08) sesiones de Dos (02) horas cada Sábado en la modalidad ON LINE.



[/accordion]