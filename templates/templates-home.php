<?php
/**
 * Template Name: Template para Inicio
 *
 * @package Polemos Politic
 * @subpackage polemospolitic-mk01-theme
 * @since Mk. 1.0
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="the-slider col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <?php $slider = get_post_meta(get_the_ID(), 'rw_slider', true); ?>
            <?php echo do_shortcode('[rev_slider alias="' . $slider . '"]'); ?>
        </section>
        <section class="the-service-line col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <?php $args = array('post_type' => 'servicios', 'posts_per_page' => 4, 'order' => 'DESC', 'orderby' => 'date', 'meta_query' => array( array( 'key' => 'rw_service_featured', 'value' => array( 1 ), 'compare' => 'IN' ))); ?>
                    <?php query_posts($args); ?>
                    <?php if (have_posts()) : ?>
                    <?php while (have_posts()) : the_post(); ?>
                    <article class="service-line-item col-lg-3 col-md-3 col-sm-3 col-xs-3 wow fadeIn delay-3">
                        <div class="col-md-4 no-paddingr">
                            <?php if ( function_exists( 'rwmb_meta' ) ) { ?>
                            <?php $icons = rwmb_meta( 'rw_service_icon', 'size=full' ); ?>
                            <?php if ( !empty( $icons ) ) { ?>
                            <?php foreach ( $icons as $image ) { ?>
                            <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                                <img src="<?php echo esc_url( $image['url'] ); ?>" title="<?php echo get_the_title(); ?>" alt="<?php echo esc_attr( $image['alt'] ); ?>" class="img-responsive service-icon" />
                            </a>
                            <?php } } } ?>
                        </div>
                        <div class="col-md-8 no-paddingl">
                            <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                                <h3><?php the_title(); ?></h3>
                            </a>
                        </div>
                    </article>
                    <?php endwhile; ?>
                    <?php endif; ?>
                    <?php wp_reset_query(); ?>
                </div>
            </div>
        </section>
        <section class="home-main-section-container col-lg-12 col-md-12 col-sm-12 col-xs-12" role="article" itemscope itemtype="http://schema.org/BlogPosting">
            <div class="container">
                <div class="row">
                    <?php $array_posts = array(); ?>
                    <div class="home-main-section col-lg-4 col-md-4 col-sm-4 col-xs-12 wow fadeIn delay-1">
                        <?php $group_value = rwmb_meta( 'sect1' ); ?>
                        <?php foreach ($group_value as $group_item) { ?>

                        <div class="home-main-section-item col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h2><?php echo $group_item['rw_title_sect1']; ?></h2>
                            <div class="section-types-container section-type1-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                <?php $cat = $group_item['rw_cat_sect1']; ?>
                                <?php $args = array('post_type' => 'post', 'posts_per_page' => $group_item['rw_num_sect1'], 'order' => 'DESC', 'orderby' => 'date', 'cat' => $cat); ?>
                                <?php query_posts($args); ?>
                                <?php if (have_posts()) : ?>
                                <?php while (have_posts()) : the_post(); ?>
                                <?php array_push($array_posts, get_the_ID());  ?>
                                <article class="section-type-item section-type1-item col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <picture>
                                        <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                                            <?php the_post_thumbnail('type1_img', array('class' => 'img-responsive')); ?>
                                        </a>
                                    </picture>
                                    <div class="section-type1-info col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                        <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title()?>">
                                            <h3><?php the_title(); ?></h3>
                                        </a>
                                        <span><?php echo get_the_date(); ?></span> <span>Por: <?php echo get_the_author(); ?></span>
                                    </div>
                                </article>
                                <?php endwhile; ?>
                                <?php endif; ?>
                                <?php wp_reset_query(); ?>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                    <div class="home-main-section col-lg-4 col-md-4 col-sm-4 col-xs-12 wow fadeIn delay-1">
                        <div class="home-main-section-item col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h2><?php echo get_post_meta(get_the_ID(), 'rw_title_sect2', true); ?></h2>
                            <div class="section-types-container section-type2-container col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                <?php $cat = get_post_meta(get_the_ID(), 'rw_cat_sect2', true); ?>
                                <?php $args = array('post_type' => 'post', 'posts_per_page' => get_post_meta(get_the_ID(), 'rw_num_sect2', true), 'order' => 'DESC', 'orderby' => 'date', 'post__not_in' => $array_posts, 'cat' => $cat); ?>
                                <?php query_posts($args); ?>
                                <?php if (have_posts()) : ?>
                                <?php $i = 1; ?>
                                <?php while (have_posts()) : the_post(); ?>
                                <?php array_push($array_posts, get_the_ID());  ?>
                                <article class="section-type-item section-type3-item col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <?php if ($i == 1) { ?>
                                    <div class="section-type3-super-item col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                        <?php the_post_thumbnail('type3_img', array('class' => 'img-responsive')); ?>
                                        <div class="section-type3-super-item-wrapper">

                                                <div class="section-type3-super-item-info col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                                                    <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title()?>">
                                                        <h3><?php the_title(); ?></h3>
                                                    </a>
                                                    <span><?php echo get_the_date(); ?></span>
                                                </div>

                                        </div>
                                    </div>
                                    <?php } else { ?>
                                    <div class="media">
                                        <div class="media-left media-middle">
                                            <a href="<?php the_permalink(); ?>">
                                                <?php the_post_thumbnail('type2_img', array('class' => 'media-object')); ?>
                                            </a>
                                        </div>
                                        <div class="media-body section-type3-info ">
                                            <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title()?>">
                                                <h4 class="media-heading"><?php the_title(); ?></h4>
                                            </a>
                                            <span>Por: <?php echo get_the_author(); ?></span>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </article>
                                <?php $i++; endwhile; ?>
                                <?php endif; ?>
                                <?php wp_reset_query(); ?>
                            </div>
                        </div>
                    </div>
                    <div class="home-main-section col-lg-4 col-md-4 col-sm-4 col-xs-12 wow fadeIn delay-1">
                        <div class="sidebar-home">
                            <?php get_sidebar(); ?>
                            <?php wp_reset_query(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="the-multimedia-section col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="multimedia-section-container col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <h2><?php _e('Multimedia', 'polemospolitic'); ?></h2>
                        <?php $args = array('post_type' => 'multimedia', 'posts_per_page' => 3, 'order' => 'DESC', 'orderby' => 'date'); ?>
                        <?php query_posts($args); ?>
                        <?php if (have_posts()) : ?>
                        <?php while (have_posts()) : the_post(); ?>
                        <div class="multimedia-item col-md-4 animated fadeIn delay-1">
                            <a href="<?php the_permalink(); ?>" title="<?php echo get_the_title(); ?>">
                                <?php the_post_thumbnail('full', array('class' => 'img-responsive')); ?>
                            </a>
                        </div>
                        <?php $i++; endwhile; ?>
                        <?php endif; ?>
                        <?php wp_reset_query(); ?>

                    </div>
                </div>
            </div>
        </section>
        <section class="the-newsletter col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
            <div class="container">
                <div class="row">
                    <div class="newsletter-container col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
                        <div class="col-md-12">
                            <h2><?php _e('SUSCRIBETE A NUESTRO BOLETÍN DE NOTICIAS', 'polemospolitic'); ?></h2>
                            <h4><?php _e('Mantente al dia del acontecer político y nuestros análisis', 'polemospolitic'); ?></h4>
                        </div>
                        <div class="col-md-12">

                            <script type="text/javascript">
                                //<![CDATA[
                                if (typeof newsletter_check !== "function") {
                                    window.newsletter_check = function (f) {
                                        var re = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-]{1,})+\.)+([a-zA-Z0-9]{2,})+$/;
                                        if (!re.test(f.elements["ne"].value)) {
                                            alert("El Correo Electrónico no es correcto");
                                            return false;
                                        }
                                        if (f.elements["nn"] && (f.elements["nn"].value == "" || f.elements["nn"].value == f.elements["nn"].defaultValue)) {
                                            alert("El Nombre esta incorrecto");
                                            return false;
                                        }
                                        if (f.elements["ns"] && (f.elements["ns"].value == "" || f.elements["ns"].value == f.elements["ns"].defaultValue)) {
                                            alert("El Apellido esta incorrecto");
                                            return false;
                                        }
                                        for (var i=1; i<20; i++) {
                                            if (f.elements["np" + i] && f.elements["np" + i].required && f.elements["np" + i].value == "") {
                                                alert("El campo es inválido");
                                                return false;
                                            }
                                        }
                                        if (f.elements["ny"] && !f.elements["ny"].checked) {
                                            alert("You must accept the privacy statement");
                                            return false;
                                        }
                                        return true;
                                    }
                                }
                                //]]>
                            </script>

                            <div class="tnp tnp-subscription">
                                <form method="post" action="http://www.polemospolitic.com/?na=s" onsubmit="return newsletter_check(this)">

                                    <table cellspacing="0" cellpadding="3" border="0">

                                        <!-- first name -->
                                        <tr>
                                            <th>Nombre</th>
                                            <td><input class="tnp-firstname" type="text" name="nn" size="30"required></td>
                                        </tr>

                                        <!-- last name -->
                                        <tr>
                                            <th>Apellido</th>
                                            <td><input class="tnp-lastname" type="text" name="ns" size="30"required></td>
                                        </tr>

                                        <!-- email -->
                                        <tr>
                                            <th>Correo Electrónico</th>
                                            <td align="left"><input class="tnp-email" type="email" name="ne" size="30" required></td>
                                        </tr>

                                        <tr>
                                            <td colspan="2" class="tnp-td-submit">
                                                <input class="tnp-submit" type="submit" value="Suscribir"/>
                                            </td>
                                        </tr>

                                    </table>
                                </form>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
