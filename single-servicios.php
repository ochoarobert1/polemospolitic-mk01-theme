<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <?php $images = rwmb_meta( 'rw_page_banner', 'size=full' );  ?>
        <?php if ( !empty( $images ) ) { ?>
        <?php foreach ( $images as $image ) { $full_url = $image['full_url']; } ?>
        <?php } ?>
        <div class="page-banner col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr animated fadeIn" style="background: url(<?php echo $full_url; ?>);">
            <div class="page-banner-wrapper"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 itemprop="headline"><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
            <div class="the-breadcrumbs col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <?php echo the_breadcrumb(); ?>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <section class="page-container col-lg-12 col-md-12 col-sm-12 col-xs-12" role="article" itemscope itemtype="http://schema.org/BlogPosting">
                    <article id="post-<?php the_ID(); ?>" class="page-content <?php echo join(' ', get_post_class()); ?>" >
                        <?php $form_servicios = get_post_meta( get_the_ID(), 'rw_service_form', 'true' ); ?>
                        <?php if ($form_servicios == 0) { ?>
                        <div class="page-article col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr" itemprop="articleBody">
                            <?php the_content(); ?>
                        </div>
                        <?php } else { ?>
                        <div class="page-article service-article col-lg-8 col-md-8 col-sm-8 col-xs-12 no-paddingl no-paddingr" itemprop="articleBody">
                            <?php the_content(); ?>
                        </div>
                        <aside class="service-form col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                            <?php $shortcode = get_post_meta( get_the_ID(), 'rw_service_shortcode', 'true' ); ?>
                            <?php echo do_shortcode($shortcode); ?>
                        </aside>
                        <?php } ?>
                    </article>
                </section>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>
