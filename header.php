<!DOCTYPE html>
<html <?php language_attributes() ?>>
    <head>
        <?php /* MAIN STUFF */ ?>
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo('charset') ?>" />
        <meta name="robots" content="NOODP, INDEX, FOLLOW" />
        <meta name="HandheldFriendly" content="True" />
        <meta name="MobileOptimized" content="320" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="pingback" href="<?php echo esc_url(get_bloginfo('pingback_url')); ?>" />
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="dns-prefetch" href="//connect.facebook.net" />
        <link rel="dns-prefetch" href="//facebook.com" />
        <link rel="dns-prefetch" href="//googleads.g.doubleclick.net" />
        <link rel="dns-prefetch" href="//pagead2.googlesyndication.com" />
        <link rel="dns-prefetch" href="//google-analytics.com" />
        <?php /* FAVICONS */ ?>
        <link rel="apple-touch-icon" href="<?php echo get_template_directory_uri(); ?>/images/apple-touch-icon.png" />
        <?php /* THEME NAVBAR COLOR */ ?>
        <meta name="msapplication-TileColor" content="#22B7C3" />
        <meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri(); ?>/images/win8-tile-icon.png" />
        <meta name="theme-color" content="#22B7C3" />
        <?php /* AUTHOR INFORMATION */ ?>
        <meta name="language" content="<?php echo get_bloginfo('language'); ?>" />
        <meta name="author" content="polemos" />
        <meta name="copyright" content="http://www.polemospolitic.com/" />
        <meta name="geo.position" content="10.4748356,-66.8609475" />
        <meta name="ICBM" content="10.4748356,-66.8609475" />
        <meta name="geo.region" content="VE" />
        <meta name="geo.placename" content="Urbanización Las Mercedes. Paseo Enrique Eraso. Torre la Noria." />
        <meta name="DC.title" content="<?php if (is_home()) { echo get_bloginfo('name') . ' | ' . get_bloginfo('description'); } else { echo get_the_title() . ' | ' . get_bloginfo('name'); } ?>" />
        <?php /* MAIN TITLE - CALL HEADER MAIN FUNCTIONS */ ?>
        <?php wp_title('|', false, 'right'); ?>
        <?php wp_head() ?>
        <?php /* OPEN GRAPHS INFO - COMMENTS SCRIPTS */ ?>
        <?php get_template_part('includes/header-oginfo'); ?>
        <?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); ?>
        <?php /* IE COMPATIBILITIES */ ?>
        <!--[if lt IE 7]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7" /><![endif]-->
        <!--[if (IE 7)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8" /><![endif]-->
        <!--[if (IE 8)&!(IEMobile)]><html <?php language_attributes(); ?> class="no-js lt-ie9" /><![endif]-->
        <!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js" /><!--<![endif]-->
        <!--[if IE]> <script type="text/javascript" src="https://cdn.jsdelivr.net/html5shiv/3.7.2/html5shiv.min.js"></script> <![endif]-->
        <!--[if IE]> <script type="text/javascript" src="https://cdn.jsdelivr.net/respond/1.4.2/respond.min.js"></script> <![endif]-->
        <!--[if IE]> <link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/favicon.ico" /> <![endif]-->
        <?php get_template_part('includes/fb-script'); ?>
        <?php get_template_part('includes/ga-script'); ?>
    </head>
    <body class="the-main-body <?php echo join(' ', get_body_class()); ?>" itemscope itemtype="http://schema.org/WebPage">
        <div id="fb-root"></div>
        <header class="container-fluid" role="banner" itemscope itemtype="http://schema.org/WPHeader">
            <div class="row">
                <div id="sticker" class="the-header col-lg-12 col-md-12 col-sm-12 col-xs-12 no-paddingl no-paddingr">
                    <div class="container">
                        <div class="row">
                            <div class="the-navbar col-lg-9 col-md-9 col-sm-9 col-xs-12 no-paddingr">
                                <nav class="navbar navbar-default" role="navigation" itemscope itemtype="http://schema.org/SiteNavigationElement">
                                    <div class="container-fluid">
                                        <!-- Brand and toggle get grouped for better mobile display -->
                                        <div class="navbar-header">
                                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                                <span class="sr-only">Toggle navigation</span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                                <span class="icon-bar"></span>
                                            </button>
                                            <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>" title="<?php _e('Ir al Inicio', 'polemospolitic'); ?>" itemscope itemtype="http://schema.org/Organization">
                                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.png" alt="<?php echo get_bloginfo('name'); ?>" class="img-responsive img-logo" />
                                            </a>
                                        </div>

                                        <!-- Collect the nav links, forms, and other content for toggling -->
                                        <?php wp_nav_menu( array( 'theme_location' => 'header_menu', 'depth' => 2, 'container' => 'div',
                                                                 'container_class' => 'collapse navbar-collapse', 'container_id' => 'bs-example-navbar-collapse-1', 'menu_class' => 'nav navbar-nav navbar-right', 'fallback_cb' => 'wp_bootstrap_navwalker::fallback', 'walker' => new wp_bootstrap_navwalker() )); ?>
                                    </div><!-- /.container-fluid -->
                                </nav>
                            </div>
                            <div class="the-search-social col-lg-3 col-md-3 col-sm-3 col-xs-12 no-paddingl">
                                <div class="the-header-form col-lg-8 col-md-8 col-sm-8 col-xs-12 no-paddingl">
                                    <form action="<?php echo home_url('/'); ?>" class="input-group custom-input-group">
                                        <input type="text" name="s" class="form-control" placeholder="<?php _e('Buscar:', 'polemospolitic'); ?>" />
                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="submit">
                                                <span class="glyphicon glyphicon-search"></span>
                                            </button>
                                        </span>
                                    </form><!-- /input-group -->
                                </div><!-- /.col-lg-6 -->
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-paddingl no-paddingr">
                                    <a href="<?php echo get_option('polemospolitic_fb'); ?>" title="<?php _e('Síguenos en Facebook', 'polemospolitic'); ?>">
                                        <i class="fa fa-facebook"></i>
                                    </a>
                                    <a href="<?php echo get_option('polemospolitic_tw'); ?>" title="<?php _e('Síguenos en Twitter', 'polemospolitic'); ?>">
                                        <i class="fa fa-twitter"></i>
                                    </a>
                                    <a href="<?php echo get_option('polemospolitic_ig'); ?>" title="<?php _e('Mira nuestro Feed', 'polemospolitic'); ?>">
                                        <i class="fa fa-instagram"></i>
                                    </a>
                                    
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
